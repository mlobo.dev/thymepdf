package com.dev4abyss.thymepdf.controller;

import com.dev4abyss.thymepdf.service.PDFService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RequestMapping("/thymeleaf")
@RestController
public class PDFController {

    private final PDFService pdfService;

    @GetMapping(value = "/pdf")
    public ResponseEntity<Resource> download() {
        return ResponseEntity.ok()
                .headers(headersPDF())
                .contentType(MediaType.APPLICATION_PDF)
                .body(pdfService.gerarPDF());
    }

    private HttpHeaders headersPDF() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + "relatorio.pdf");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        String mimetype = "application/pdf";
        headers.add(HttpHeaders.CONTENT_TYPE, mimetype);
        return headers;
    }
}
