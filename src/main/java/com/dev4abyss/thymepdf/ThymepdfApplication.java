package com.dev4abyss.thymepdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymepdfApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymepdfApplication.class, args);
	}

}
