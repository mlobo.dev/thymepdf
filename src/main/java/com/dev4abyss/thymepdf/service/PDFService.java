package com.dev4abyss.thymepdf.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

@RequiredArgsConstructor
@Service
@Log4j2
public class PDFService {

    private final TemplateEngine templateEngine;

    private String parseThymeleafTemplate() {

        Context context = new Context();
        context.setVariable("to", "Thymeleaf ");

        return templateEngine.process("index", context);
    }

    @SneakyThrows
    public InputStreamResource gerarPDF() {
        String html = parseThymeleafTemplate();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(os);
        InputStream inputStream = new ByteArrayInputStream(os.toByteArray());
        return new InputStreamResource(inputStream);

    }
}
